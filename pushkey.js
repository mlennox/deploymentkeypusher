#!/usr/bin/env node
/**
 * Makes OAuth connection to BitBucket API
 * and pushes deployment key to specified repo
 *
 * two-legged workflow is here http://oauthbible.com/#oauth-10a-two-legged
 * 		consumer asks for request token using signed request
 * 		server responds with request token and request secret
 * 		consumer sends reponse token for access token using signed request
 * 		service sends access token and access secret
 * 		consumer uses access token and secret to access endpoints
 *
 * when token expires? Doesn't concern us as we are only making one POST
 */


// var OAuthClient = require('client-oauth');
var Cmdr = require('commander');
var Promise = require('bluebird');
var QueryString = require('query-string');
var Base64 = require('base-64');
var utf8 = require('utf8');
var https = require('https');

Cmdr
  .version('1.0.0')
  // except these are not optional...
  .option('-k, --key <>', 'Consumer key')
  .option('-s, --secret <>', 'Consumer secret')
  .option('-d, --deploymentkey <>', 'the Public key we will push to bitbucket that allows read-only deployment access to the repo')
  .option('-a, --account_name <>', 'the bitbucket account name')
  .option('-r, --repo <>', 'The target repo slug')
  .parse(process.argv);

// because we are pushing this to BitBucket we know the URLs
// var baseUrl = 'https://bitbucket.org/api/1.0/oauth',
// 	requestTokenUrl = baseUrl + '/request_token',
// 	accessTokenUrl = baseUrl + '/access_token',
// 	deploymentKeysUrl = '';

var baseUrl = 'https://bitbucket.org/site/oauth2',
	authoriseUrl = '/authorize',
	accessTokenUrl = '/access_token';

function encodeAuthHeader(client_key, client_secret){
	return Base64.encode(utf8.encode(client_key + ':' + client_secret));
}

function authorise(client_key, client_secret) {
	var authHeader = 'Basic ' + encodeAuthHeader(client_key, client_secret);
	console.log('auth header', authHeader);
	return httpRequest({
		        host: baseUrl,
		        path: authoriseUrl,
		        headers: {
		        	'Authorization': authHeader
		        }
		    });
}

function fetchAccessKey() {

}

function pushDeploymentKey(requestData) {
	console.log('got here', requestData);
	// Cmdr.deploymentkey
	// Cmdr.account_name
	// Cmdr.slug
	//POST https://bitbucket.org/api/1.0/repositories/{accountname}/{repo_slug}/deploy-keys --data "key=value"
}

function httpRequest(options) {
	return new Promise(function(resolve, reject){
		// try {
			https.get(options, function(response) {
				// response.setEncoding('utf8');
		        // Continuously update stream with data
		        var body = '';
		        response.on('data', function(chunk) {
		            body += chunk;
		        });
		        response.on('end', function() {
		        	console.log('body', body);
		        	resolve(body);
		        });
		    });
		// }
		// catch (err){
		// 	reject(err);
		// }
	});
}

// function clientPost(client, url, extraData) {
// 	return new Promise(function(resolve, reject){
// 		try {
// 			client.post(url, extraData, function (err, data, response){
// 				// console.log('url', url);
// 				// console.log('err', err);
// 				// console.log('data', data);
// 				console.log('statusCode', response.statusCode);

// 				if (err){
// 					reject(err);
// 				}

// 				// seems reasonable to assume status code should be 200
// 				if (response && response.statusCode && response.statusCode === 200){
// 					resolve(data);
// 				} else {
// 					reject(data);
// 				}


// 			});
// 		}
// 		catch (err){
// 			reject(err);
// 		}
// 	});
// }

// finally found proper workflow documentation, hopefully this works...
// https://bitbucket.org/site/master/issues/11520/oauth-2-authentication-does-not-work

function push() {

	console.log('key', Cmdr.key);

	authorise(Cmdr.key, Cmdr.secret)
		.then(fetchAccessKey)
		.then(pushDeploymentKey);
		// .catch(function(err){
		// 	console.log('there was some problem : ', err);
		// });

	// createClient({
	// 	key: Cmdr.key,
	// 	oauth_consumer_key: Cmdr.secret,
	// 	secret: Cmdr.secret,
	// 	oauth_callback: '127.0.0.1'
	// })
	// 	.then(getRequestToken)
	// 	.then(getAccessToken)
	// 	.then(pushDeploymentKey)
	// 	.catch(function(err){
	// 		// do something
	// 		console.log('error!', err);
	// 	});
}

// kick it off
push();

// module.exports = {
// 	push: push
// }